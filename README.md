# Проект - Composites
# Прогнозирование конечных свойств композитных материалов

## Краткое описание
Программный код создан в `Jupyter Notebook` на языке `Python`.
Использованы следующие библиотеки:  
`pandas`, `numpy`, `sklearn`, `matplotlib`, `seaborn`, `random`, `tensorflow`, `keras`, `flask`.

В файле `Code_vkr.ipynb` находится основной проект.  
Датасеты находится в папке - `data`.

В файле `Flask_app.ipynb` находится код приложения осуществляющего прогнозирование.  
Модель для прогнозирования находится в папке - `model`.

## Этапы проекта
### Введение
В данной работе показаны возможности применения инструментов Data Science в одной из важнейшей на сегодняшний день отрасли как «Композиционные материалы». Проект представляет возможные способы решения задачи прогнозирования конечных свойств композитных материалов на основе представленных характеристик входящих в его состав компонентов.

### Постановка задачи
Актуальность решаемой задачи заключается в создании прогнозной модели, которая в свою очередь поможет сократить количество проводимых испытаний, а также пополнить базу данных материалов возможными новыми характеристиками материалов, и цифровыми двойниками новых композитов.
В качестве исходного материала для анализа данных представлен набор из двух датасетов (две таблицы в формате MS Excel). В качестве входной информации указаны данные о начальных свойствах компонентов композиционных материалов (количество связующего, наполнителя, температурный режим отверждения и т.д.). В качестве выходной информации необходимо спрогнозировать ряд конечных свойств получаемых композиционных материалов.
Для выполнения поставленной задачи прогнозирования требуется создать и обучить модель предсказывать три переменные из тринадцати, а именно: соотношение матрица-наполнитель, модуль упругости при растяжении и прочность при растяжении.

### Разведочный анализ данных
Загрузка и ознакомление с данными о свойствах компонентов композиционных материалов.
Подготовка датафрейма и проведение разведочного анализ данных:  
- Статистические данные исходной выборки для каждой переменной  
- Гистограммы частот по каждой переменной  
- Диаграмма размаха по каждой переменной (box-plot)  
- Попарные графики рассеивания  
- Коэффициенты корреляции в виде тепловой карты  

### Предобработка данных
- Проверка на выбросы по каждой переменной  
- Нормализация данных  
- Проверка по гистограммам частот и диаграммам размаха

### Разработка и обучение модели
Исходя из основной решаемой задачи, а именно прогнозирование конечных свойств композиционного материала, а также представленных данных в которых все значения переменных имеют количественный характер, следует применять методы регрессионного анализа. Регрессионный анализ исследует влияния одной или нескольких независимых переменных на зависимую переменную.
Цели регрессионного анализа:
-	Определение степени детерминированности вариации зависимой переменной независимыми переменными;
-	Предсказание значения зависимой переменной с помощью независимых;
-	Определение вклада отдельных независимых переменных в вариацию зависимой.

Процесс распределения данных на входные значения (независимые переменные) и выходные значения (зависимые переменные).

Используемые методы (модели):
- Линейная регрессия (LinearRegression)  
- Метод регрессии Лассо (Lasso)
- Решающее дерево (DecisionTreeRegressor)
- Случайный лес (RandomForestRegressor)
- K-ближайших соседей (KNeighborsRegressor)

### Тестирование модели
Процесс тестирования обученной модели.  
Процедура подачи входных тестовых данных и получение выходных данных.  
Сравнение с реальными данными и оценка точности прогнозирования обученной модели.

### Разработка нейронной сети 
Построение нейронной сеть для прогнозирования третьей переменной: соотношение матрица-наполнитель.  
Для конструирования нейронной сети используется библиотека «Tensorflow».
